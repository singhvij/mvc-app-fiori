sap.ui.define([
		"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
	], function (Controller, JSONModel) {
	"use strict";
	// debugger;
	return Controller.extend("sapui5.demo.mvcapp.controller.Detail", {
		onNavPress: function () {
			oApp.back();
		}
	});
});